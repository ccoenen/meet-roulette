/*global JitsiMeetExternalAPI, luxon */
const nametag = document.getElementById('name');
let nickname = 'unknown';
let meetingConclusion = null;
let config;
let jitsiApi;

async function loadJitsiAPI() {
	const response = await fetch('/config.json');
	if (response.ok) {
		config = await response.json();
	} else {
		console.log('could not load config', response);
	}
}
loadJitsiAPI();

function getNickNameFromLocalStorage() {
	const currentData = JSON.parse(localStorage.getItem('features/base/settings'));
	if (currentData) {
		if (currentData.displayName) {
			nickname = currentData.displayName;
			nametag.value = currentData.displayName;
		}
	}
}

function setNickNameToLocalStorage() {
	let currentData = JSON.parse(localStorage.getItem('features/base/settings'));
	if (!currentData) {
		currentData = {};
	}
	currentData.displayName = nickname;
	localStorage.setItem('features/base/settings', JSON.stringify(currentData));
}

getNickNameFromLocalStorage();




nametag.addEventListener('input', (e) => {
	nickname = e.target.value;
	setNickNameToLocalStorage();
	sendNickname();
});


let socket;
function connectSocket() {
	console.debug('attempting websocket connection');
	socket = new WebSocket(window.location.toString().replace(/^http/, 'ws'));
	socket.addEventListener('open', () => {
		document.querySelector('div.status').innerText = 'connected';
		sendNickname();
	});
	socket.addEventListener('message', (message) => {
		const data = JSON.parse(message.data);

		if (data.participants) {
			renderParticipants(data.participants);
		}
		if (data.meeting) {
			joinMeeting(data);
		}
		if (data.secondaryURL) {
			displaySecondaryURL(data.secondaryURL);
		}
	});
	socket.addEventListener('close', () => {
		document.querySelector('div.status').innerText = 'disconnected';
		renderParticipants([]);
		setTimeout(connectSocket, 1000);
	});
}
connectSocket();


function sendNickname() {
	if (socket.readyState === WebSocket.OPEN) {
		socket.send(JSON.stringify({nickname}));
	}
}


function renderParticipants(participants) {
	const element = document.querySelector('aside ul');
	const parent = element.parentElement;
	parent.removeChild(element);
	element.innerHTML = '';
	participants.forEach(p => {
		const item = document.createElement('LI');
		item.innerText = p;
		element.appendChild(item);
	});
	parent.appendChild(element);
}

function renderMeetingClock() {
	if (!meetingConclusion) return;
	const target = document.getElementById('countdown-clock');
	target.innerText = meetingConclusion.diff(luxon.DateTime.local()).toFormat('mm:ss');
}
setInterval(renderMeetingClock, 1000);

function joinMeeting(meetingData) {
	const node = document.querySelector('main');
	if (jitsiApi) {
		jitsiApi.dispose();
	}

	meetingConclusion = luxon.DateTime.fromISO(meetingData.conclusion);

	jitsiApi = new JitsiMeetExternalAPI(config.jitsiHost, {
		roomName: meetingData.meeting,
		width: '100%',
		height: '100%',
		parentNode: node,
		// userInfo: {
		// 	displayName: "something",
		// 	email: "something@example.com"
		// },
		configOverwrite: {
			disableInviteFunctions: true,
			doNotStoreRoom: true
		},
		interfaceConfigOverwrite: {
			// DEFAULT_BACKGROUND: '#ff0000', // works
			DEFAULT_LOGO_URL: 'none',
			DEFAULT_REMOTE_DISPLAY_NAME: 'Studi',
			DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
			DISABLE_PRESENCE_STATUS: true,
			DISABLE_VIDEO_BACKGROUND: true,
			ENABLE_DIAL_OUT: false,
			// filmStripOnly: true, // this is a weird mode that we do not want.
			HIDE_DEEP_LINKING_LOGO: true,
			JITSI_WATERMARK_LINK: 'https://imd.mediencampus.h-da.de',
			MOBILE_APP_PROMO: false,
			NATIVE_APP_NAME: 'test',
			PROVIDER_NAME: 'IMD',
			SETTINGS_SECTIONS: [],
			SHOW_JITSI_WATERMARK: false,
			SHOW_WATERMARK_FOR_GUESTS: false,
			TOOLBAR_BUTTONS: [
				'microphone', 'camera', 'fullscreen',
				'fodeviceselection', 'profile', 'chat',
				'settings', 'raisehand',
				'videoquality', 'feedback', 'stats', 'shortcuts',
				'tileview', 'help', 'mute-everyone'
			],
		}
	});
	jitsiApi.executeCommand('subject', 'DEMO SUBJECT');
	jitsiApi.addEventListener('tileViewChanged', (e) => {
		console.log(e);
	});
	jitsiApi.executeCommand('toggleTileView');
}

function displaySecondaryURL(url) {
	console.log(`received secondary URL: ${url}`);
	const aside = document.querySelector('aside.video')
	const iframe = document.createElement('iframe');
	aside.innerHTML = '';
	iframe.setAttribute('src', url);
	const rect = aside.getBoundingClientRect();
	iframe.setAttribute('width', rect.width);
	iframe.setAttribute('height', rect.height);
	aside.appendChild(iframe);
}
