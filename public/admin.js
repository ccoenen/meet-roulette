document.querySelector('#shuffle').addEventListener('click', (e) => {
	var url = new URL(location);
	url.pathname = url.pathname.replace('/admin', '/shuffle');

	var params = {
		interval: document.querySelector('#interval').value,
		minimumMeetingSize: document.querySelector('#minimumMeetingSize').value
	};
	url.search = new URLSearchParams(params).toString();

	fetch(url, {
		method: 'post',
	});
});

document.querySelector('#update-secondary-iframe').addEventListener('click', (e) => {
	var url = new URL(location);
	url.pathname = url.pathname.replace('/admin', '/secondary-url');

	var params = {
		url: document.querySelector('#secondary-iframe-url').value
	};
	url.search = new URLSearchParams(params).toString();

	fetch(url, {
		method: 'post',
	});
});
