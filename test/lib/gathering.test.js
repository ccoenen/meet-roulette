const assert = require('assert');

const Gathering = require('../../lib/gathering');
const Participant = require('../../lib/participant');

function prepareGathering(n) {
	const g = new Gathering('test');
	for (let i = 0; i < n; i++) {
		const p = new Participant();
		g.participants.push(p);
	}
	return g;
}

describe('Gathering', () => {
	it('can be instantiated', () => {
		const g = new Gathering('test');
		assert(g, 'we can create an instance');
	});

	describe('splitting', () => {
		it('gatherings are split up evenly into bite-sized meetings', () => {
			const g = prepareGathering(20);
			g.minimumMeetingSize = 5;
			const groups = g.getRandomGroups();
			const count = groups.reduce((accumulator, meeting) => accumulator + meeting.length, 0);
			assert.strictEqual(groups.length, 4);
			assert.strictEqual(count, 20, 'everyone should be in a meeting');

			g.minimumMeetingSize = 10;
			const groups2 = g.getRandomGroups();
			const count2 = groups.reduce((accumulator, meeting) => accumulator + meeting.length, 0);
			assert.strictEqual(groups2.length, 2);
			assert.strictEqual(count2, 20, 'everyone should be in a meeting');
		});

		it('leaves noone alone', () => {
			const g = prepareGathering(22);
			g.minimumMeetingSize = 7;
			const groups = g.getRandomGroups();
			const count = groups.reduce((accumulator, meeting) => accumulator + meeting.length, 0);
			assert.strictEqual(groups.length, 3);
			assert.strictEqual(groups[0].length, 8);
			assert.strictEqual(groups[1].length, 7);
			assert.strictEqual(groups[2].length, 7);
			assert.strictEqual(count, 22, 'everyone should be in a meeting');
		});

		it('needs twice minimum size to actually form a second group', () => {
			const g = prepareGathering(7);
			g.minimumMeetingSize = 4;
			const groups = g.getRandomGroups();
			const count = groups.reduce((accumulator, meeting) => accumulator + meeting.length, 0);
			assert.strictEqual(groups.length, 1);
			assert.strictEqual(groups[0].length, 7);
			assert.strictEqual(count, 7, 'everyone should be in a meeting');
		});
	});
});
