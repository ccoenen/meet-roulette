const assert = require('assert');

const Participant = require('../../lib/participant');

describe('Participant', function () {
	it('can be instantiated', function () {
		const p = new Participant();
		assert(p, 'we can create an instance');
	});
});
