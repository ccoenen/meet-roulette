const path = require('path');

const express = require('express');
const expressWs = require('express-ws');

const Gathering = require('./lib/gathering');
const Participant = require('./lib/participant');
const routeProxyExternalAPI = require('./lib/route-proxy-external-api');

const config = require('./config.json');

const gathering = new Gathering('roulette');
const app = express();
expressWs(app);

const publicDir = path.join(__dirname, 'public');
app.use(express.static(publicDir));
app.use('/', routeProxyExternalAPI(config));

app.get('/config.json', (req, res) => {
	res.json(config);
});

app.get('/:gatheringId/admin', (req, res) => {
	res.sendFile('admin.html', { root: publicDir });
});

app.get('/:gatheringId', (req, res) => {
	res.sendFile('index.html', { root: publicDir });
});

app.post('/:gatheringId/shuffle', (req, res) => {
	if (req.query.interval) {
		gathering.gatheringInterval = parseInt(req.query.interval, 10);
	}
	if (req.query.minimumMeetingSize) {
		gathering.minimumMeetingSize = parseInt(req.query.minimumMeetingSize, 10);
	}
	gathering.shuffleParticipants();
	res.end('OK');
});

app.post('/:gatheringId/secondary-url', (req, res) => {
	if (req.query.url) {
		console.log(`URL received: ${req.query.url}`);
		gathering.setSecondaryURL(req.query.url);
	}
	res.end('OK');
});

app.ws('/:gatheringId', (client, request) => {
	console.log(`client connected to ${request.params.gatheringId}`);

	const p = new Participant(client);
	gathering.join(p);

	client.on('message', (data) => {
		let message;
		try {
			message = JSON.parse(data);
		} catch (e) {
			console.error('message could not be parsed: %s\n%s', e, data);
			return;
		}
		if (message.nickname) {
			p.nickname = message.nickname;
			gathering.announceParticipants();
		}
	});

	client.on('close', () => {
		console.log('client disconnected');
		gathering.partConnection(client);
	});
});

app.listen(3000);
