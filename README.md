# Meet Roulette

A Chatroulette-Like thing that uses Jitsi-Meet.

People come toghether in a gathering, and are then sorted into individual meetings. The number of participants per meeting is configurable.


## Requirements

* node.js / NPM


## set up (running your own daemon)

* run `npm install`
* run `npm start`


## set up (as docker container)

* run `docker-compose build`
* run `docker-compose up -d`


## Internals

* **Gathering**: A bunch of people meet to be then sorted into meetings.
* **Meeting**: The same term as in Jitsi Meet - a few people that are currently talking to each other.
* **Participant**: Someone who is part of a gathering.
* **Lobby**: A waiting area


## Communication

### Websocket

Server may at any time send a message to switch to a specified meeting or to the lobby.

Client may at any time connect, disconnect or switch names of a participant.

Server may at any time send a list of currently connected participants.

### REST-API

Client may request a configuration (not yet sure if needed)

Client (admin) may set basic settings like duration of meetings, number of participants per meeting


## Todo

- [x] should always start in tiled mode
- [ ] admin should be able to set time and trigger a reshuffle
- [ ] joining late should still join you into a meeting
- [ ] video stream player for music or similar broadcast things
- [ ] test: does this work for anyone with no prior cookies under this domain?
- [ ] topics as conversation starters
- [ ] statistics endpoint
