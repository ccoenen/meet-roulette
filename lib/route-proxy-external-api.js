const https = require('https');

const express = require('express');

function getRouter(config) {
	const router = express.Router();

	router.get('/external_api.js', (req, res) => {
		console.log(`proxying request to ${config.jitsiHost}.`);
	
		const options = {
			host: config.jitsiHost,
			port: 443,
			path: '/external_api.js',
		};
	
		https
			.request(options, pres => {
				pres.setEncoding('utf8');
	
				res.writeHead(pres.statusCode);
	
				pres.on('data', chunk => { res.write(chunk); });
				pres.on('close', () => { res.end(); });
				pres.on('end', () => { res.end(); });
			})
			.on('error', e => {
				try {
					// attempt to set error message and http status
					console.error(`proxying request to ${config.jitsiHost} failed: ${e.message}`);
					res.writeHead(500);
					res.write(e.message);
				} catch (e) {
					console.error(`proxying request to ${config.jitsiHost} failed AGAIN: ${e.message}`);
				}
				res.end();
			})
			.end();
	});

	return router;
}

module.exports = getRouter;
