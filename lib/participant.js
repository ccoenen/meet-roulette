class Participant {
	constructor(connection) {
		this.connection = connection;
		this.nickname = 'unknown';
		this.subgroup = 'none';
	}
}

module.exports = Participant;
