const DateTime = require('luxon').DateTime;
const _ = require('underscore');

class Gathering {
	constructor(name) {
		this.name = name;
		this.participants = [];

		this.minimumMeetingSize = 4; // about this many participants per meeting. Actual number may vary.
		this.gatheringInterval = 300; // seconds
		this.conclusion = null;
		this.timeout = null;

		this.secondaryURL = null;
	}

	join(p) {
		this.participants.push(p);
		this.announceParticipants();
		if (this.secondaryURL) {
			const payload = JSON.stringify({secondaryURL: this.secondaryURL});
			p.connection.send(payload);
		}
	}

	broadcast(payload) {
		this.participants.forEach(p => {
			p.connection.send(payload);
		});
	}

	partConnection(connection) {
		const index = this.participants.findIndex(candidate => candidate.connection === connection);
		if (index > -1) {
			this.participants.splice(index, 1);
			this.announceParticipants();
		} else {
			console.error('this participant is not part of this gathering');
		}
	}

	announceParticipants() {
		const names = this.participants.map(p => p.nickname);
		const payload = JSON.stringify({participants: names});
		this.broadcast(payload);
	}

	getRandomGroups() {
		const groups = _.chunk(_.shuffle(this.participants), this.minimumMeetingSize);

		if (groups.length < 2) {
			return groups;
		}
		const lastGroup = groups.pop();
		if (lastGroup.length >= this.minimumMeetingSize) {
			groups.push(lastGroup);
			return groups;
		}
		lastGroup.forEach((p, index) => {
			groups[index % groups.length].push(p);
		});
		return groups;
	}

	shuffleParticipants() {
		console.log(`shuffle for ${this.name}`);
		this.getRandomGroups().forEach((meeting, index) => {
			const meetingId = `rouletteG${this.name}R${index + 1}`;
			this.setConclusionTimer(this.gatheringInterval);
			const payload = JSON.stringify({
				meeting: meetingId,
				participants: meeting.map(p => p.nickname),
				conclusion: this.conclusion.toISO()
			});
			meeting.forEach(p => {
				p.subgroup = index + 1;
				p.connection.send(payload);
			});
		});
	}

	setConclusionTimer(seconds) {
		this.conclusion = DateTime.local().plus({seconds});
		clearTimeout(this.timeout);
		this.timeout = setTimeout(() => { this.shuffleParticipants(); }, seconds * 1000);
	}

	setSecondaryURL(url) {
		this.secondaryURL = url;
		const payload = JSON.stringify({secondaryURL: url});
		this.broadcast(payload);
	}
}

module.exports = Gathering;
